/*
warning "rollup-plugin-vue > vue-template-validator@1.1.5" has incorrect peer dependency "parse5@^2.1.0".
warning " > rollup-plugin-vue@3.0.0" has unmet peer dependency "vue-template-compiler@*".
*/

import Vue from "vue";
import VueTimeago from 'vue-timeago';

import App from "./App.vue"

Vue.use(VueTimeago, {
    name: 'timeago', // component name, `timeago` by default
    locale: 'en-US',
    locales: {
      // you will need json-loader in webpack 1
      'en-US': [
        "just now",
        ["%s second ago", "%s seconds ago"],
        ["%s minute ago", "%s minutes ago"],
        ["%s hour ago", "%s hours ago"],
        ["%s day ago", "%s days ago"],
        ["%s week ago", "%s weeks ago"],
        ["%s month ago", "%s months ago"],
        ["%s year ago", "%s years ago"]
      ]
    }
  })


Vue.config.productionTip = false

// I had to find this in a fkng comment on github
// https://github.com/vuejs-templates/webpack/issues/215#issuecomment-287652462
new Vue(App).$mount('#app')
